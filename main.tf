module "instance" {
    source = "./modules/instance"
    public_subnets = module.vpc.public_subnets
}

module "vpc" {
    source = "./modules/vpc"
}