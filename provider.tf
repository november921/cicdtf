terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "s3" {
    bucket = "gitlab-test-shaqs-terraform-backend"
    key    = "terraform.tfstate"
    region = "us-east-2"
  }

}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"
}