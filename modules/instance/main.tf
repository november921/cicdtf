data "aws_ami" "this" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "architecture"
    values = ["arm64"]
  }
  filter {
    name   = "name"
    values = ["al2023-ami-2023*"]
  }
}

resource "aws_instance" "example" {
  ami = "ami-01387af90a62e3c01"
  instance_type = "t2.micro"

  subnet_id = var.public_subnets[0]
  tags = {
    Name = "test-machine-1"
  }
}